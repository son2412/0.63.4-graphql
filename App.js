import React from 'react';
import RootContainer from './src/Root/RootContainer.Screen';
import 'react-native-gesture-handler';
import { ApolloProvider } from '@apollo/client';
import client from './src/apollo';
import ContextProvider from './src/context';
import 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer, persistStore } from 'redux-persist';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import rootReducer from './src/reducers';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './src/reducers';

const sagaMiddleware = createSagaMiddleware();
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: []
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const middleware = [thunk];
const logger = createLogger();
middleware.push(logger);
const store = createStore(persistedReducer, applyMiddleware(...middleware));
// const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware, createLogger()));
const persistor = persistStore(store);
// sagaMiddleware.run(rootSaga);
const App = () => {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ContextProvider>
            <RootContainer />
          </ContextProvider>
        </PersistGate>
      </Provider>
    </ApolloProvider>
  );
};
export default App;
