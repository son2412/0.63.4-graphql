const colors = {
  primary: '#009387',
  white: '#ffffff',
  black: '#000000',
  charcoalGrey: '#30333a',
  boldGrey: '#919191',
  grey: '#d8d8d8',
  bgRoot: '#f4f4f6',
  red: '#ee3b3b',
  border: 'rgba(0, 0, 0, 0.1)',
  backgroundColor: '#ecf0f2',
  headerTextColor: '#9ca0a9',
  blueDotColor: '#3f87df',
  redDotColor: '#e8634f',
  yellowDotColor: '#ffc815',
  greenDotColor: '#5b9b6d',
  blackColor: '#000000',
  tranparent: 'rgba(0,0,0,0)',
  blue: '#5f82e6',
  dark: '#000',
  light: '#f6f6f6'
};

export default colors;
