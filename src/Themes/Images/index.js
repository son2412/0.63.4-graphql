const Images = {
  AlarmClock: require('../Images/alarm-clock.png'),
  Bell: require('../Images/bell.png'),
  Bluetooth: require('../Images/bluetooth.png'),
  Comment: require('../Images/comment.png'),
  Email: require('../Images/email.png'),
  Fastbackward: require('../Images/fast-backward.png'),
  Fastforward: require('../Images/fast-forward.png'),
  Menu: require('../Images/menu.png'),
  Notification: require('../Images/notification.png'),
  Rain: require('../Images/rain.png'),
  Pause: require('../Images/pause.png'),
  PhoneCall: require('../Images/phone-call.png'),
  Settings: require('../Images/settings.png'),
  SpeakerFilledAudioTool: require('../Images/speaker-filled-audio-tool.png'),
  Wifi: require('../Images/wifi.png'),
  // ---------------TravelUI---------------- //
  Asia: require('../Images/Asia.jpg'),
  Africe: require('../Images/africe.jpg'),
  Algeria: require('../Images/algeria.jpg'),
  Antarctica: require('../Images/antarctica.jpg'),
  Australia: require('../Images/australia.jpg'),
  Me: require('../Images/iliesOuldmenouer.jpg'),
  Italia: require('../Images/italy.jpg'),
  Maldive: require('../Images/maldive.jpg'),
  Norway: require('../Images/norway.jpg'),
  Seoul: require('../Images/seoul.jpg'),

  Avatar: require('../Images/avatar.png')
};
export default Images;
