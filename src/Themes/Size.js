import { create } from 'react-native-pixel-perfect';
const designResolution = {
  width: 390,
  height: 844
};
const perfectSize = create(designResolution);

export default perfectSize;
