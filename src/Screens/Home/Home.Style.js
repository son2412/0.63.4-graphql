import { StyleSheet } from 'react-native';
import { fontFamily, fontSize } from '../../const';
import ApplicationStyle from '../../Themes/Application.Style';
import colors from '../../Themes/Colors';

export default StyleSheet.create({
  ...ApplicationStyle,
  btnGetData: {
    backgroundColor: colors.charcoalGrey,
    width: 120,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 5,
    alignSelf: 'center'
  },
  textGetData: {
    // fontFamily: fontFamily.regular,
    color: colors.white,
    fontSize: fontSize.medium
  }
});
