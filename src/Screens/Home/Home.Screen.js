import React from 'react';
import { ActivityIndicator, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import styles from './Home.Style';
import colors from '../../Themes/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { barStyle } from '../../const';
import { useNavigation } from '@react-navigation/native';
import CallList from '../../Components/CallList/CallList';
import { useQuery } from '@apollo/client';
import { LIST_GROUP_REQUEST } from '../../builder';
import { useDispatch } from 'react-redux';
import { sendNetworkFail } from '../../actions';

const HomeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { loading, error, data } = useQuery(LIST_GROUP_REQUEST);

  if (error && error.networkError) {
    dispatch(sendNetworkFail(error.networkError));
  }
  const renderToolbar = () => {
    return (
      <View style={styles.toolbar}>
        <StatusBar hidden={false} backgroundColor={colors.primary} barStyle={barStyle.lightContent} />
        <TouchableOpacity style={styles.viewWrapIcLeft} onPress={navigation.openDrawer}>
          <MaterialCommunityIcons name={'menu'} size={30} color={colors.white} />
        </TouchableOpacity>
        <View style={styles.viewWrapTitleToolbar}>
          <Text style={styles.titleToolbar}>Home</Text>
        </View>
        <View style={styles.viewWrapIcRight} />
      </View>
    );
  };

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="small" />
      </View>
    );
  }

  return (
    <View style={styles.mainContainer}>
      {renderToolbar()}
      <CallList groups={data.groups.data} />
    </View>
  );
};
export default HomeScreen;
