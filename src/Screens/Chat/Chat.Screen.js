import React, { Fragment, useCallback, useContext, useEffect, useState } from 'react';
import { StatusBar, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import styles from './Chat.Style';
import colors from '../../Themes/Colors';
import { barStyle } from '../../const';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import AppStyles from '../../styles';
import { GiftedChat, InputToolbar } from 'react-native-gifted-chat';
import { CTX } from '../../context';
import { useMutation, useQuery, useSubscription } from '@apollo/client';
import { ADD_MESSAGE_REQUEST, LIST_MESSAGE_REQUEST, SUBSCRIPTION_MESSAGE } from '../../builder';

const ChatScreen = (props) => {
  const context = useContext(CTX);
  const { user } = context;
  const { group_name, group } = props.route.params;
  const [messages, setMessages] = useState([]);
  const [page, setPage] = useState(1);
  const navigation = useNavigation();
  const onPress = () => {};
  const [addMessage, resultAddMessage] = useMutation(ADD_MESSAGE_REQUEST);
  const subscription = useSubscription(SUBSCRIPTION_MESSAGE, { variables: { topic: `${group.id}` } });
  const { loading, error, data } = useQuery(LIST_MESSAGE_REQUEST, { variables: { group_id: +group.id, pageIndex: page, pageSize: 20 } });
  useEffect(() => {
    if (!loading && data && !error) {
      setMessages((prrevious) => [
        ...prrevious,
        ...data.messages.data.map((m) => {
          return { _id: +m.id, text: m.message, createdAt: m.created_at, user: { _id: +m.sender.id, avatar: m.sender.avatar } };
        })
      ]);
    }
  }, [loading, data, error]);
  const _onSend = (text) => {
    addMessage({ variables: { group_id: +group.id, message: text[0].text } });
  };

  useEffect(() => {
    if (subscription.data && !subscription.loading && !subscription.error) {
      const message = subscription.data.subscriptionMessageToDynamicTopic;
      setMessages((previousMessages) => [
        {
          _id: +message.id,
          text: message.message,
          crearedAt: message.created_at,
          user: { _id: +message.sender.id, avatar: message.sender.avatar }
        },
        ...previousMessages
      ]);
    }
  }, [subscription.loading, subscription.data, subscription.error]);

  useEffect(() => {
    console.log(page);
  }, [page]);

  const renderToolbar = () => {
    return (
      <View style={styles.toolbar}>
        <StatusBar hidden={false} backgroundColor={colors.primary} barStyle={barStyle.lightContent} />
        <TouchableOpacity style={styles.viewWrapIcLeft} onPress={() => navigation.goBack()}>
          <MaterialCommunityIcons name={'arrow-left'} size={30} color={colors.white} />
        </TouchableOpacity>
        <View style={styles.viewWrapTitleToolbar}>
          <Text style={styles.titleToolbar}>{group_name}</Text>
        </View>
        <View style={styles.viewWrapIcRight}>
          <TouchableRipple onPress={onPress} style={styles.icon} rippleColor="rgba(0, 0, 0, .32)">
            <Icon size={24} color={AppStyles.colors.accentColor} name="call" />
          </TouchableRipple>
          <TouchableRipple onPress={onPress} style={styles.icon} rippleColor="rgba(0, 0, 0, .32)">
            <Icon size={24} color={AppStyles.colors.accentColor} name="videocam" />
          </TouchableRipple>
        </View>
      </View>
    );
  };

  // const renderAction = () => {
  //   return (
  //     <TouchableRipple
  //       onPress={onPress}
  //       style={styles.icon}
  //       rippleColor="rgba(0, 0, 0, .32)">
  //       <Icon size={24} color={AppStyles.colors.accentColor} name="image" />
  //     </TouchableRipple>
  //   );
  // };

  const renderSend = (prop) => {
    const { text, onSend } = prop;
    return (
      <TouchableRipple onPress={() => onSend({ text: text.trim() }, true)} style={styles.icon} rippleColor="rgba(0, 0, 0, .32)">
        <Icon size={28} color={AppStyles.colors.accentColor} name="send" />
      </TouchableRipple>
    );
  };

  const customtInputToolbar = (prop) => {
    return (
      <InputToolbar
        {...prop}
        containerStyle={{
          backgroundColor: 'white',
          borderTopColor: '#E8E8E8',
          borderTopWidth: 1,
          padding: 3
        }}
      />
    );
  };

  const loadMoreMessage = () => {
    setPage((previous) => previous + 1);
  };

  return (
    <Fragment>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainContainer}>
          {renderToolbar()}
          <GiftedChat
            messages={messages}
            onSend={(text) => _onSend(text)}
            user={{ _id: +user.id }}
            placeholder={'Aa'}
            alwaysShowSend={true}
            isKeyboardInternallyHandled={false}
            renderInputToolbar={(prop) => customtInputToolbar(prop)}
            renderSend={(prop) => renderSend(prop)}
            listViewProps={{
              onEndReached: loadMoreMessage,
              onEndReachedThreshold: 1000
            }}
          />
        </View>
      </SafeAreaView>
    </Fragment>
  );
};
export default ChatScreen;
