import React, { useContext, useEffect, useState } from 'react';
import styles from './RootContainer.Style';
import { ActivityIndicator, Keyboard, Platform, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RootStackScreen from './RootStack.Screen';
import { CTX } from '../context';
import DrawerNavigatorScreen from '../Components/Drawer/DrawerNavigator.Screen';
import HomeScreen from '../Screens/Home/Home.Screen';
import DashboardScreen from '../Components/RentalUI/DashboardScreen';
import DetailScreen from '../Components/RentalUI/DetailScreen';
import { useQuery } from '@apollo/client';
import { PROFILE_REQUEST } from '../builder';
import { useDispatch, useSelector } from 'react-redux';
import { clearNetworkFail, checkSignIn } from '../actions';
import ChatScreen from '../Screens/Chat/Chat.Screen';

const Stack = createStackNavigator();

const RootContainerScreen = () => {
  const context = useContext(CTX);
  const { token, _profile } = context;
  const sendNetworkFail = useSelector((state) => state.sendNetworkFail);
  const isLogin = useSelector((state) => state.isAuth);
  const dispatch = useDispatch();
  const clearNetworkStatus = () => dispatch(clearNetworkFail());
  const [isKeyboardShow, setIsKeyboardShow] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      setIsKeyboardShow(true);
      setKeyboardHeight(e.endCoordinates.height);
    });
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setIsKeyboardShow(false);
    });

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);

  if (sendNetworkFail.err) {
    switch (sendNetworkFail.err) {
      case 'NETWORK_ERROR':
        // Toast.show('No network connection, please try again');
        break;
      case 'TIMEOUT_ERROR':
        // Toast.show('Timeout, please try again');
        break;
      case 'CONNECTION_ERROR':
        // Toast.show('DNS server not found, please try again');
        break;
      default:
        // Toast.show(sendNetworkFail.err);
        break;
    }
    clearNetworkStatus();
  }

  const { loading, error, data } = useQuery(PROFILE_REQUEST);

  useEffect(() => {
    if (!loading && !error && data) {
      _profile(data.me);
    }
  }, [_profile, data, error, loading]);

  useEffect(() => {
    setTimeout(async () => {
      dispatch(checkSignIn(token));
    }, 10);
  }, [dispatch, token]);

  if (isLogin.fetching) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="small" />
      </View>
    );
  }

  return (
    <View style={styles.mainContainer}>
      <NavigationContainer>
        {token ? (
          <Stack.Navigator initialRouteName="Drawer" headerMode={'none'}>
            <>
              <Stack.Screen name="Drawer" component={DrawerNavigatorScreen} options={{ gestureEnabled: true, gestureDirection: 'horizontal' }} />
              <Stack.Screen name="Home" component={HomeScreen} options={{ gestureEnabled: true, gestureDirection: 'horizontal' }} />
              <Stack.Screen name="Dashboard" component={DashboardScreen} options={{ gestureEnabled: true, gestureDirection: 'horizontal' }} />
              <Stack.Screen name="DetailRental" component={DetailScreen} options={{ gestureEnabled: true, gestureDirection: 'horizontal' }} />
              <Stack.Screen name="Chat" component={ChatScreen} options={{ gestureEnabled: true, gestureDirection: 'horizontal' }} />
            </>
          </Stack.Navigator>
        ) : (
          <>
            <RootStackScreen />
          </>
        )}
      </NavigationContainer>

      {isKeyboardShow && Platform.OS === 'ios' ? <View style={{ height: keyboardHeight }} /> : null}
    </View>
  );
};
export default RootContainerScreen;
