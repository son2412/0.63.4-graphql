import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SignInScreen from '../Screens/SignIn/SignIn.Screen';

const RootStack = createStackNavigator();

const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="SignIn" component={SignInScreen} />
  </RootStack.Navigator>
);

export default RootStackScreen;
