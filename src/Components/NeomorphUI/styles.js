import { StyleSheet } from 'react-native';
import Colors from '../../Themes/Colors';
import perfectSize from '../../Themes/Size';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
    marginTop: -90
  },
  header: {
    height: perfectSize(50),
    width: '100%',
    marginTop: perfectSize(100),
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  headerText: {
    color: Colors.headerTextColor,
    fontSize: perfectSize(12)
  },
  heaerEndSection: {
    height: perfectSize(30),
    width: perfectSize(90),
    backgroundColor: Colors.backgroundColor,
    shadowRadius: 10,
    borderRadius: perfectSize(23),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  hearNotifactionIcon: {
    height: perfectSize(18),
    width: perfectSize(19),
    tintColor: Colors.headerTextColor
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 23,
    backgroundColor: Colors.blueDotColor
  },
  red: {
    height: 10,
    width: 10,
    borderRadius: 23,
    backgroundColor: Colors.redDotColor
  },
  green: {
    height: 10,
    width: 10,
    borderRadius: 23,
    backgroundColor: Colors.greenDotColor
  },
  yellow: {
    height: 10,
    width: 10,
    borderRadius: 23,
    backgroundColor: Colors.yellowDotColor
  },
  headerDate: {
    color: Colors.headerTextColor,
    textAlign: 'right',
    fontSize: perfectSize(18),
    right: perfectSize(36),
    marginTop: perfectSize(14)
  },
  menuItems: {
    height: perfectSize(156),
    width: perfectSize(150),
    backgroundColor: Colors.backgroundColor,
    shadowRadius: 10,
    borderRadius: 23,
    alignItems: 'center'
  },
  menuIcons: {
    height: perfectSize(50),
    width: perfectSize(50),
    backgroundColor: Colors.backgroundColor,
    shadowRadius: 10,
    borderRadius: 23,
    marginRight: 10,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  menuIcons2: {
    height: perfectSize(50),
    width: perfectSize(50),
    backgroundColor: Colors.backgroundColor,
    shadowRadius: 10,
    borderRadius: 23,
    marginRight: 10,
    marginLeft: 10
  },
  icon: {
    height: perfectSize(25),
    width: perfectSize(25)
  },
  end: {
    height: perfectSize(50),
    width: perfectSize(300),
    alignSelf: 'center',
    backgroundColor: Colors.backgroundColor,
    shadowRadius: 10,
    borderRadius: 23,
    marginTop: perfectSize(23),
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row'
  },
  endIcon: {
    height: perfectSize(18),
    width: perfectSize(18),
    tintColor: Colors.headerTextColor
  }
});
