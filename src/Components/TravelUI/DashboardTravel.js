import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Explore from './Explore';
import Travel from './Travel';
import Saved from './Saved';
import Profile from './Profile';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

const CustomTabBarBottom = ({ children, onPress }) => {
  return (
    <TouchableOpacity style={{ top: -17, justifyContent: 'center', ...styles.shadow }} onPress={onPress}>
      <View style={{ width: 70, height: 70, borderRadius: 35, backgroundColor: '#e32f45' }}>{children}</View>
    </TouchableOpacity>
  );
};
export default function DashboardTravel() {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          bottom: 10,
          left: 20,
          right: 20,
          elevation: 0,
          backgroundColor: '#ffffff',
          borderRadius: 15,
          height: 70,
          ...styles.shadow
        }
      }}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Explore',
          tabBarIcon: ({ color, size }) => (
            <View style={styles.tabIcon}>
              <MaterialCommunityIcons name="compass-outline" color={color} size={size} />
            </View>
          )
        }}
        name="Explore"
        component={Explore}
      />

      <Tab.Screen
        options={{
          tabBarLabel: 'Saved',
          tabBarIcon: ({ color, size }) => (
            <View style={styles.tabIcon}>
              <MaterialCommunityIcons name="bookmark-outline" color={color} size={size} />
            </View>
          )
        }}
        name="Saved"
        component={Saved}
      />

      <Tab.Screen
        options={{
          tabBarLabel: 'Plus',
          tabBarIcon: ({ color, size }) => <MaterialCommunityIcons name="plus" color={color} size={35} />,
          tabBarButton: (props) => <CustomTabBarBottom {...props} />
        }}
        name="Plus"
        component={Saved}
      />

      <Tab.Screen
        options={{
          tabBarLabel: 'Travel',
          tabBarIcon: ({ color, size }) => (
            <View style={styles.tabIcon}>
              <MaterialCommunityIcons name="airplane" color={color} size={size} />
            </View>
          )
        }}
        name="Travel"
        component={Travel}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <View style={styles.tabIcon}>
              <MaterialCommunityIcons name="bag-personal-outline" color={color} size={size} />
            </View>
          )
        }}
        name="Profile"
        component={Profile}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5
  },
  tabIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 20
  }
});
