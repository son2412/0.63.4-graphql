import React from 'react';
import { View, TouchableOpacity, Text, Image, TextInput, FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Images from '../../Themes/Images';
import styles from './styles';
import Evaluation from './Evaluation';
const listOfCountries = [
  { title: 'Asia', imgPath: Images.Asia },
  { title: 'Australia', imgPath: Images.Australia },
  { title: 'Antarctica', imgPath: Images.Antarctica },
  { title: 'Algeria', imgPath: Images.Algeria }
];
const listOfCountriesRecommend = [
  { title: 'Italy', imgPath: Images.Italia },
  { title: 'Maldive', imgPath: Images.Maldive },
  { title: 'Norway', imgPath: Images.Norway },
  { title: 'Seoul', imgPath: Images.Seoul }
];
export default function Explore() {
  const navigation = useNavigation();
  const renderHeader = () => {
    return (
      <View style={styles.mainHeader}>
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 17 }}>EXPLORE</Text>
            <View style={{ marginHorizontal: 5 }}>
              <MaterialCommunityIcons name="chevron-down" color={'black'} size={22} />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.dot} />
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image style={styles.avatar} source={Images.Me} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const renderSearch = () => {
    return (
      <View style={styles.searchView}>
        <View style={styles.iconSearch}>
          <MaterialCommunityIcons name="map-search" color={'grey'} size={20} />
        </View>
        <TextInput style={styles.seacrBox} placeholder={'Search for destinations'} />
      </View>
    );
  };
  const renderItemHorizontal = ({ item }) => {
    return (
      <View style={styles.itemHorizontal}>
        <View style={{ position: 'absolute' }}>
          <Image source={item.imgPath} style={styles.imageHorizontal} />
          <View style={styles.backgroundHorizontal} />
        </View>
        <Text style={{ color: 'white', fontSize: 21 }}>{item.title}</Text>
      </View>
    );
  };
  const renderHorizontal = () => {
    return (
      <View style={{ marginVertical: 10 }}>
        <FlatList horizontal keyExtractor={(item) => item.title} data={listOfCountries} renderItem={renderItemHorizontal} />
      </View>
    );
  };
  const renderItemFeaturePlace = ({ item }) => {
    return (
      <View style={styles.itemFeature}>
        <View style={{ position: 'absolute' }}>
          <Image source={item.imgPath} style={{ height: 220, width: 150, borderRadius: 10 }} />
          <View style={styles.backgroundFeature} />
        </View>
        <View style={{ margin: 10 }}>
          <Text style={{ color: 'white', fontSize: 21 }}>{item.title}</Text>
          <View>
            <Evaluation rating={4} />
          </View>
        </View>
      </View>
    );
  };
  const renderFeaturePlace = () => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 20 }}>
          <Text style={{ flex: 1, fontWeight: 'bold', fontSize: 17 }}>Featured places</Text>
          <Text style={{ color: '#3498db', fontWeight: 'bold' }}>View All</Text>
        </View>
        <View>
          <FlatList horizontal keyExtractor={(item) => item.title} data={listOfCountries} renderItem={renderItemFeaturePlace} />
        </View>
      </View>
    );
  };
  const renderRecommend = () => {
    return (
      <View>
        <View style={{ flexDirection: 'row', paddingHorizontal: 20 }}>
          <Text style={{ flex: 1, fontWeight: 'bold', fontSize: 17 }}>Recommendations</Text>
          <Text style={{ color: '#3498db', fontWeight: 'bold' }}>View All</Text>
        </View>

        <View>
          {listOfCountriesRecommend.map((elment) => {
            return (
              <View style={styles.itemRecommend} key={elment.title}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image style={styles.imageRecommend} source={elment.imgPath} />
                  <View>
                    <Text style={{ fontWeight: 'bold' }}>{elment.title}</Text>
                    <Evaluation />
                  </View>
                  <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <View style={styles.backgroundRecommend}>
                      <MaterialCommunityIcons name="bookmark-multiple" color={'#f0932b'} size={22} />
                    </View>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </View>
    );
  };
  return (
    <ScrollView>
      <View style={{ backgroundColor: '#ecf0f1', flex: 1 }}>
        {renderHeader()}
        {renderSearch()}
        {renderHorizontal()}
        {renderFeaturePlace()}
        {renderRecommend()}
      </View>
    </ScrollView>
  );
}
