import { StyleSheet } from 'react-native';
export default StyleSheet.create({
  mainHeader: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center'
  },
  dot: {
    height: 10,
    width: 10,
    backgroundColor: 'green',
    borderRadius: 5,
    position: 'absolute',
    elevation: 2,
    right: 0
  },
  avatar: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2
  },
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4
  },
  iconSearch: {
    // paddingLeft: -30,
    elevation: 1,
    position: 'absolute',
    left: 20
  },
  seacrBox: {
    height: 50,
    marginHorizontal: 10,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    paddingLeft: 50,
    flex: 1
  },
  itemHorizontal: {
    height: 70,
    width: 110,
    backgroundColor: 'red',
    margin: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 2
  },
  imageHorizontal: {
    height: 70,
    width: 110,
    borderRadius: 10
  },
  backgroundHorizontal: {
    backgroundColor: 'black',
    position: 'absolute',
    height: '100%',
    width: '100%',
    opacity: 0.3,
    borderRadius: 10
  },
  itemFeature: {
    height: 220,
    width: 150,
    backgroundColor: 'red',
    margin: 10,
    borderRadius: 10,
    //   alignItems: 'center',
    //   justifyContent: 'center',
    elevation: 2
  },
  backgroundFeature: {
    backgroundColor: 'black',
    position: 'absolute',
    height: '100%',
    width: '100%',
    opacity: 0.3,
    borderRadius: 10
  },
  itemRecommend: {
    height: 100,
    backgroundColor: 'white',
    marginVertical: 5,
    marginHorizontal: 20,
    // alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2
  },
  imageRecommend: {
    height: 70,
    width: 70,
    borderRadius: 8,
    margin: 10
  },
  backgroundRecommend: {
    backgroundColor: 'white',
    height: 40,
    width: 40,
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6
  }
});
