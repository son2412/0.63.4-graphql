import { createDrawerNavigator } from '@react-navigation/drawer';
import React, { Fragment } from 'react';
import colors from '../../Themes/Colors';
import styles from './DrawerNavigator.Style';
import { SafeAreaView } from 'react-native';
import DrawerContentScreen from './DrawerContent.Screen';
import HomeScreen from '../../Screens/Home/Home.Screen';
import Dashboard from '../NeomorphUI/Dashboard';
import DashboardTravel from '../TravelUI/DashboardTravel';
import OnBoardScreen from '../RentalUI/OnBoardScreen';

const Drawer = createDrawerNavigator();

const DrawerNavigatorScreen = () => {
  return (
    <Fragment>
      <SafeAreaView style={{ flex: 1 }}>
        <Drawer.Navigator
          drawerContent={() => <DrawerContentScreen />}
          drawerContentOptions={{
            activeTintColor: colors.primary,
            labelStyle: styles.textItemMenu
          }}>
          <Drawer.Screen name="HomeScreen" component={HomeScreen} options={{ drawerLabel: 'Home' }} />
          <Drawer.Screen name="NeomorphUI" component={Dashboard} options={{ drawerLabel: 'NeomorphUI' }} />
          <Drawer.Screen name="TravelUI" component={DashboardTravel} options={{ drawerLabel: 'TravelUI' }} />
          <Drawer.Screen name="RentalUI" component={OnBoardScreen} options={{ drawerLabel: 'RentalUI' }} />
        </Drawer.Navigator>
      </SafeAreaView>
    </Fragment>
  );
};
export default DrawerNavigatorScreen;
