import React, { useContext } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { useTheme, Drawer, Text, TouchableRipple, Switch } from 'react-native-paper';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './DrawerNavigator.Style';
import { CTX } from '../../context';
import Profile from './Profile/Profile';

const DrawerContentScreen = () => {
  const paperTheme = useTheme();
  const navigation = useNavigation();
  const context = useContext(CTX);
  const { _logout, user } = context;
  const logOut = () => {
    _logout();
  };
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView>
        <View style={styles.drawerContent}>
          {!user ? <ActivityIndicator size="small" /> : <Profile profile={user} />}
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({ color, size }) => <Icon name="home-outline" color={color} size={size} />}
              label="Home"
              onPress={() => navigation.navigate('HomeScreen')}
            />
            <DrawerItem
              icon={({ color, size }) => <Icon name="bookmark-outline" color={color} size={size} />}
              label="NeomorphUI"
              onPress={() => {
                navigation.navigate('NeomorphUI');
              }}
            />
            <DrawerItem
              icon={({ color, size }) => <Icon name="bookmark-outline" color={color} size={size} />}
              label="TravelUI"
              onPress={() => {
                navigation.navigate('TravelUI');
              }}
            />
            <DrawerItem
              icon={({ color, size }) => <Icon name="bookmark-outline" color={color} size={size} />}
              label="RentalUI"
              onPress={() => {
                navigation.navigate('RentalUI');
              }}
            />
          </Drawer.Section>
          <Drawer.Section title="Preferences">
            <TouchableRipple onPress={() => {}}>
              <View style={styles.preference}>
                <Text>Dark Theme</Text>
                <View pointerEvents="none">
                  <Switch value={paperTheme.dark} />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem icon={({ color, size }) => <Icon name="exit-to-app" color={color} size={size} />} onPress={logOut} label="Sign Out" />
      </Drawer.Section>
    </View>
  );
};
export default DrawerContentScreen;
