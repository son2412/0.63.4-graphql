import React, { useContext } from 'react';
import { View } from 'react-native';
import { TouchableRipple, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Avatar from '../Avatar/Avatar';
import AppStyles from '../../styles';
import styles from './styles';
import { CTX } from '../../context';
import { useNavigation } from '@react-navigation/native';

const CallItem = ({ item }) => {
  const context = useContext(CTX);
  const { user } = context;
  const navigation = useNavigation();
  const onPress = () => {
    navigation.navigate('Chat', { group: item, group_name: renderItem().name });
  };

  const renderItem = () => {
    const find = item.users.find((x) => +x.id !== user.id);
    return {
      name: `${find.first_name} ${find.last_name}`,
      avatar: find.avatar
    };
  };

  return (
    <TouchableRipple onPress={onPress} rippleColor="rgba(0, 0, 0, .32)">
      <View style={styles.item}>
        <Avatar uri={renderItem().avatar} />
        <View style={styles.nameView}>
          <Text style={styles.head}>{renderItem().name}</Text>
          <Text style={styles.sub}>@{renderItem().name.toLowerCase()}</Text>
        </View>
        <TouchableRipple onPress={onPress} style={styles.icon} rippleColor="rgba(0, 0, 0, .32)">
          <Icon size={24} color={AppStyles.colors.accentColor} name="call" />
        </TouchableRipple>
        <TouchableRipple onPress={onPress} style={styles.icon} rippleColor="rgba(0, 0, 0, .32)">
          <Icon size={24} color={AppStyles.colors.accentColor} name="videocam" />
        </TouchableRipple>
      </View>
    </TouchableRipple>
  );
};

export default CallItem;
