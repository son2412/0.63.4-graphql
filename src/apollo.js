import { ApolloClient, createHttpLink, InMemoryCache, split } from '@apollo/client';
import configs from './config';
import { setContext } from '@apollo/client/link/context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { onError } from '@apollo/client/link/error';

const errorLink = new onError(({ operation, response, graphQLErrors, networkError, forward }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) => {
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
      return forward(operation);
    });
  }

  if (networkError) {
    console.log(`[Network error]: ${networkError}`);
    return forward(operation);
  }
});
const httpLink = createHttpLink({
  uri: configs.apiGraphQlDomain
});
const authLink = setContext(async (_, { headers }) => {
  const token = await AsyncStorage.getItem('userToken');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  };
});
const wsLink = new WebSocketLink({
  uri: configs.subscriptionGraphQl,
  options: {
    reconnect: true
  }
});
const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
  },
  wsLink,
  httpLink,
  errorLink
);
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(splitLink)
});

export default client;
