const configs = {
  apiGraphQlDomain: 'http://13.212.177.234:3001/graphql',
  apiRESTDomain: 'http://13.212.177.234:3000/api/v1',
  subscriptionGraphQl: 'ws://13.212.177.234:3001/subscriptions',
  firebase: {
    apiKey: 'AIzaSyAfXzdlGIgeuylEg_aTex9KItr5XjSvMGo',
    authDomain: 'chatfirebase-c6acb.firebaseapp.com',
    databaseURL: 'https://chatfirebase-c6acb.firebaseio.com',
    projectId: 'chatfirebase-c6acb',
    storageBucket: 'chatfirebase-c6acb.appspot.com',
    messagingSenderId: '535909429783',
    appId: '1:535909429783:web:3323de1f1bb4ee0ffde252'
  }
};

export default configs;
