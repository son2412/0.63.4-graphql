import { combineReducers } from 'redux';
import { CLEAR_NETWORK_FAIL, SEND_NETWORK_FAIL, RETRIEVE_TOKEN, SIGN_OUT } from './actions';

const initialState = { fetching: false, data: null, err: null };

const sendNetworkFail = (state = initialState, action) => {
  switch (action.type) {
    case SEND_NETWORK_FAIL:
      return {
        err: action.payload.err
      };
    case CLEAR_NETWORK_FAIL:
      return {
        err: null
      };
    default:
      return state;
  }
};
const initialStateAuth = { fetching: true, token: null };
const isAuth = (state = initialStateAuth, action) => {
  switch (action.type) {
    case RETRIEVE_TOKEN:
      return {
        token: action.payload.token,
        fetching: false
      };
    case SIGN_OUT:
      return {
        token: null,
        fetching: false
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  sendNetworkFail,
  isAuth
});
export default rootReducer;
