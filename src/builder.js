import { gql } from '@apollo/client';

export const SIGNIN_REQUEST = gql`
  mutation SignIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
      user {
        id
      }
    }
  }
`;

export const LIST_GROUP_REQUEST = gql`
  query Groups {
    groups(data: { pageIndex: 1 }) {
      data {
        id
        users {
          id
          first_name
          last_name
          avatar
        }
      }
    }
  }
`;

export const PROFILE_REQUEST = gql`
  query Profile {
    me {
      id
      first_name
      last_name
      avatar
      email
    }
  }
`;

export const SUBSCRIPTION_MESSAGE = gql`
  subscription onSubscriptionMessageToDynamicTopic($topic: String!) {
    subscriptionMessageToDynamicTopic(topic: $topic) {
      id
      sender_id
      group_id
      type
      created_at
      message
      sender {
        id
        avatar
      }
    }
  }
`;

export const LIST_MESSAGE_REQUEST = gql`
  query Messages($group_id: Float!, $pageIndex: Float!, $pageSize: Float!) {
    messages(group_id: $group_id, data: { pageIndex: $pageIndex, pageSize: $pageSize }) {
      data {
        id
        message
        created_at
        sender {
          id
          avatar
        }
      }
      totalRow
    }
  }
`;

export const ADD_MESSAGE_REQUEST = gql`
  mutation Message($group_id: Float!, $message: String!) {
    message(data: { group_id: $group_id, message: $message })
  }
`;
